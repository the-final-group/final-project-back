#!/bin/bash

# Variables
BUCKET_NAME="unique-mongodb-backup-bucket-12345"
#"unique-mongodb-backup-bucket-12345"
DATE=$(date +"%Y-%m-%d-%H-%M")
BACKUP_NAME="mongodb-backup-$DATE"
MONGO_URI="mongodb+srv://rachelpytse:OyicQMFZO9l0t6eY@cluster0.pf4av5q.mongodb.net/test"
BACKUP_DIR="/var/backups/mongodb"

# Créer un répertoire de sauvegarde temporaire
mkdir -p $BACKUP_DIR/$BACKUP_NAME

# Effectuer la sauvegarde
sudo mongodump --uri="$MONGO_URI" --out=$BACKUP_DIR/$BACKUP_NAME

# Télécharger la sauvegarde vers S3
aws s3 cp $BACKUP_DIR/$BACKUP_NAME s3://$BUCKET_NAME/ --recursive

echo "Sauvegarde MongoDB effectuée avec succès et téléchargée vers S3."
