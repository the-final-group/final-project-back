variable "environment" {
  description = "The domain name to manage"
  type        = string
  default     = "preprod"
}

variable "key" {
  type        = string
  description = "SSH key name"
  default     = "project_key"
}

variable "public_subnet_cidr" {
  description = "Public subnet CIDR values"
  default     = "10.0.0.0/24"
}

variable "private_subnet_cidr" {
  description = "Private subnet CIDR values"
  default     = "10.0.1.0/24"
}

variable "azs" {
  type        = list(string)
  description = "Availability zone"
  default     = ["eu-north-1a"]
}

# variable "script" {
#   default = <<-EOF
# #!/bin/bash -v
# sudo sed -i 's/^#Port 22/Port 2046/' /etc/ssh/sshd_config
# sudo service sshd restart
# EOF
# }


variable "ovh_application_key" {
  description = "OVH API application key"
  type        = string
}

variable "ovh_application_secret" {
  description = "OVH API application secret"
  type        = string
}

variable "ovh_consumer_key" {
  description = "OVH API consumer key"
  type        = string
}

variable "domain_name" {
  description = "The domain name to manage"
  type        = string
}

# Access variables  ------- >

variable "access_key" {
  description = "AWS access key"
  type        = string
}

variable "secret_key" {
  description = "AWS secret key"
  type        = string
}

variable "region" {
  description = "AWS region"
  type        = string
  default     = "eu-north-1" 
}


# -----  Backend bucket
variable "backend_bucket" {
  description = "The name of the S3 bucket to be created"
  type        = string
  default     = "tf-backend-morningnews-preprod"
}


