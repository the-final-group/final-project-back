# Déploiement de l'infrastructure

## Lancez le script de création du bucket

```bash
cd terraform/preprod/scripts
terraform init
terraform apply
```

## Lancez la création des ressources

```bash
cd terraform/preprod
terraform init
terraform apply
```

## Récuperez la clé pour vous connecter a l'instance

On récupere sa valeur depuis AWS secret manager et on la stock dans un fichier en local:

```bash
aws secretsmanager get-secret-value --secret-id morningnews_back_private_key --region eu-north-1 --query SecretString --output text > morningnews_back_key_pair.pem
```

On restreint les droit du nouveau fichier enregistré:

```bash
chmod 400 "morningnews_back_key_pair.pem"
```

Besoin de supprimer la clé immediatement ?

```bash
aws secretsmanager delete-secret \
    --secret-id private_key_secret \
    --force-delete-without-recovery \
    --region eu-north-1
```