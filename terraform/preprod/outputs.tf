
# ---------- Network
output "vpc_id" {
  value = aws_vpc.main.id
  description = "The ID of the VPC"
}


output "public_subnet_cidr_block" {
  value = aws_subnet.public_subnet.cidr_block
  description = "List of IDs of public subnets"
}

output "public_subnet_id" {
  value = aws_subnet.public_subnet.id
  description = "List of IDs of public subnets"
}

output "private_subnet_id" {
  value = aws_subnet.private_subnet.id
  description = "List of IDs of public subnets"
}

output "internet_gw" {
  value = aws_internet_gateway.gw.id
  description = "List of IDs of internet gateway"
}

# ---------- Security

output "security_group_id" {
  value = aws_security_group.project_sg_backend.id
  description = "ID of the shared security group"
}

# ---------- Instances

output "backend_preprod_id" {
  value = aws_instance.morningnews_back_vm.id
  description = "ID of the preprod EC2 instance"
}

output "backend_preprod_ip" {
  value       = aws_instance.morningnews_back_vm.public_ip
  description = "Public IP address of the public EC2 instances."
}

output "instance_dns" {
  value       = ovh_domain_zone_record.sub_morningnews
  description = "DNS config."
}

output "private_key_path" {
  value = local_sensitive_file.private_key.filename
}